package com.spextreme.examples.tdd.shape;

/**
 * An abstract shape to provide most of the functionality needed by other shapes.
 */
public abstract class AbstractShape {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  /**
   * Constructs this object.
   */
  public AbstractShape() {}

  /**
   * Calculates the area.
   *
   * @return The area.
   */
  public abstract double getArea();

  /**
   * Gets the height.
   *
   * @return The height.
   */
  public int getHeight() {
    return height;
  }

  /**
   * Gets the width.
   *
   * @return The width.
   */
  public int getWidth() {
    return width;
  }

  /**
   * Gets the x.
   *
   * @return The x.
   */
  public int getX() {
    return x;
  }

  /**
   * Gets the y.
   *
   * @return The y.
   */
  public int getY() {
    return y;
  }

  /**
   * Sets the height.
   *
   * @param height The height to set.
   */
  public void setHeight(int height) {
    this.height = height;
  }

  /**
   * Sets the width.
   *
   * @param width The width to set.
   */
  public void setWidth(int width) {
    this.width = width;
  }

  /**
   * Sets the x.
   *
   * @param x The x to set.
   */
  public void setX(int x) {
    this.x = x;
  }


  /**
   * Sets the y.
   *
   * @param y The y to set.
   */
  public void setY(int y) {
    this.y = y;
  }

}

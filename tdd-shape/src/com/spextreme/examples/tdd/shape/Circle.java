package com.spextreme.examples.tdd.shape;

public class Circle extends AbstractShape {

  @Override
  public double getArea() {
    return Math.PI * Math.pow((getWidth() / 2), 2);
  }
}

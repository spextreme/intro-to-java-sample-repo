package com.spextreme.examples.tdd.shape;

public class Rectangle extends AbstractShape {

  @Override
  public double getArea() {
    return getHeight() * getWidth();
  }
}

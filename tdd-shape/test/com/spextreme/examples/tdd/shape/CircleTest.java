package com.spextreme.examples.tdd.shape;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.spextreme.examples.tdd.shape.Circle;
import org.junit.jupiter.api.Test;

class CircleTest {

  @Test
  void testGetArea() {
    final Circle circle = new Circle();

    circle.setHeight(1);
    circle.setWidth(1);

    // The 5 checks how far in the precision of the result should be matched.
    assertEquals(Math.PI, circle.getArea(), 5);
  }
}

package com.spextreme.examples.tdd.shape;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.spextreme.examples.tdd.shape.Rectangle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AbstractShapeTest {

  private Rectangle rectangle = null;

  @BeforeEach
  void setUp() throws Exception {
    rectangle = new Rectangle();
  }

  @AfterEach
  void tearDown() throws Exception {}

  @Test
  void testPositionXandY() {
    assertEquals(0, rectangle.getX());
    assertEquals(0, rectangle.getY());

    rectangle.setX(10);
    rectangle.setY(50);

    assertEquals(10, rectangle.getX());
    assertEquals(50, rectangle.getY());
  }

  @Test
  void testWidthAndHeight() {
    assertEquals(0, rectangle.getWidth());
    assertEquals(0, rectangle.getHeight());

    rectangle.setWidth(25);
    rectangle.setHeight(45);

    assertEquals(25, rectangle.getWidth());
    assertEquals(45, rectangle.getHeight());
  }
}

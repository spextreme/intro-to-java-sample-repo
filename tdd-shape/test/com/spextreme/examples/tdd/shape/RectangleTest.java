package com.spextreme.examples.tdd.shape;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.spextreme.examples.tdd.shape.Rectangle;
import org.junit.jupiter.api.Test;

class RectangleTest {

  @Test
  void testGetArea() {
    final Rectangle rectangle = new Rectangle();

    rectangle.setHeight(2);
    rectangle.setWidth(5);

    assertEquals(10, rectangle.getArea());
  }
}

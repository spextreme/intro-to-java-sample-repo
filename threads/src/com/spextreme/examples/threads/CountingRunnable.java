package com.spextreme.examples.threads;

/**
 * A simple seconds counter.
 */
public class CountingRunnable implements Runnable {

  /**
   * {@inheritDoc}.
   */
  @Override
  public void run() {

    for (int i = 1; i <= 60; i++) {

      System.out.println("[Counter Thread] " + i + " s");

      try {

        Thread.sleep(1000);

      } catch (final InterruptedException e) {
        // ignore since we're just using it to pause.
      }
    }
  }

}

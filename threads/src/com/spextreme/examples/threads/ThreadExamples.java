package com.spextreme.examples.threads;

/**
 *
 */
public class ThreadExamples {

  public static void main(String[] args) {

    final ThreadExamples examples = new ThreadExamples();

    System.out.println("Starting both threads:");
    examples.spawnSecondsThread();
    examples.spawnCountByTensThread();
  }

  public void spawnCountByTensThread() {

    System.out.println("Starting Tens Thread:");
    final Thread thread = new Thread((Runnable) () -> {
      for (int i = 1; i <= 100000; i *= 10) {

        System.out.println("[Tens Thread] " + i);

        try {

          Thread.sleep(5000);

        } catch (final InterruptedException e) {
        }
      }

    }, "Tens");

    thread.start();
  }

  public void spawnSecondsThread() {

    System.out.println("Starting Seconds Counter:");

    final Thread thread = new Thread(new CountingRunnable(), "Counter");

    thread.start();
  }
}

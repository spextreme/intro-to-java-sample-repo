package com.spextreme.examples.scope;

public class ShapeProtected {

  // A field defined as protected.
  protected int width = 0;
}

package com.spextreme.examples.scope;

public class Square extends ShapeProtected {

  // A get method defined for the protected shape width field.
  public int getWidth() {
    return width;
  }

  // A set method defined for the protected shape width field.
  public void setWidth(int width) {
    this.width = width;
  }
}

package com.spextreme.examples.scope;

public class ShapePublic {

  // A field defined as public.
  public int width = 0;

  // A get method defined as public.
  public int getWidth() {
    return width;
  }
}

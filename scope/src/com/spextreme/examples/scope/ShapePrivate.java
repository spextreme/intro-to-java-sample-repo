package com.spextreme.examples.scope;

public class ShapePrivate {

  // A field defined as private.
  private int width = 0;

  // A get method defined as public to allow access to the field.
  public int getWidth() {
    return width;
  }

  // A set method to update the private field.
  public void setWidth(int width) {
    this.width = width;
  }
}

package com.spextreme.examples.scope;

public class ShapePackagePrivate {

  // A field defined as package private.
  int width = 0;

  // A get method defined as public to allow access to the field.
  public int getWidth() {
    return width;
  }

  // a set method to update the field but only things in this package (scope) can access it.
  void setWidth(int width) {
    this.width = width;
  }
}

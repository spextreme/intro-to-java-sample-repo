package com.spextreme.examples.recursion;

/**
 * A set of recursion call examples.
 */
public class RecursionExamples {

  public static void main(String[] args) {

    final RecursionExamples examples = new RecursionExamples();

    // Uncomment this to run and see how far it gets before terminating.
    // examples.infiniteRecursion(0);

    examples.limitedRecursion(1);
  }

  public void infiniteRecursion(int counter) {
    System.out.println(String.format("infiniteRecursion(%s)", counter));

    infiniteRecursion(++counter);
  }


  public void limitedRecursion(int counter) {

    System.out.println(String.format("limitedRecursion(%s)", counter));

    if (counter >= 10) {
      return;
    }

    limitedRecursion(++counter);
  }

}

package com.spextreme.examples.anonymousclass;

/**
 * 
 */
public class StandardClass {

  public void runThread() throws Exception {
    
    Thread t = new Thread(new Runnable() {

      @Override
      public void run() {

        System.out.print("Anonymous class Runnable for a simpel thread execution complete.");
      }
    });

    t.start();
  }
}

package com.spextreme.examples.innerclass;

public class NormalClass {

  /**
   * The inner class defined internally to the Normal class (outer class).
   */
  private class InnerClass {

    public String name;
    public int value;

    public InnerClass(String name, int value) {
      this.name = name;
      this.value = value;
    }
  }

  private String altName;
  private final InnerClass inner;

  public NormalClass(String name, String altName, int value) {
    inner = new InnerClass(name, value);
  }

  public String getAltName() {
    return altName;
  }

  public InnerClass getInner() {
    return inner;
  }

  public String getName() {
    return getInner().name;
  }

  public int getValue() {
    return inner.value;
  }
}

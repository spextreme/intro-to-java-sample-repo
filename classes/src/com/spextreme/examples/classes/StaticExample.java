package com.spextreme.examples.classes;

/**
 * A class that uses all static fields/methods for examples.
 */
public class StaticExample {
  /**
   * A static string to print out in the main method.
   */
  public static String helloString = "Hello World";

  /**
   * JVM launch point to print hello world.
   *
   * @param args The command line args. Not used.
   */
  public static void main(String[] args) {
    System.out.println(helloString);
  }

  /**
   * A simple utility method that see if the word hello appears in the string.
   *
   * @param message The message to check.
   * @return <code>true</code> if this has the word hello in the message.
   */
  public static boolean saysHello(String message) {
    return message.toLowerCase().contains("hello");
  }
}

package com.spextreme.examples.classes;

import java.io.File;

/**
 *
 */
public class Configuration {

  private int autoSaveRate = -1;
  private String dbConnStr = null;
  private String dbType = null;

  /**
   * Gets the autoSaveRate.
   *
   * @return The autoSaveRate.
   */
  public int getAutoSaveRate() {
    return autoSaveRate;
  }

  /**
   * Gets the dbConnStr.
   *
   * @return The dbConnStr.
   */
  public String getDbConnStr() {
    return dbConnStr;
  }


  /**
   * Gets the dbType.
   *
   * @return The dbType.
   */
  public String getDbType() {
    return dbType;
  }

  public void initialize() {

    dbType = "";
    dbConnStr = "";
    autoSaveRate = 15;
  }

  public void initialize(File f) {
    // Read from file and set values.
  }

  public void initialize(String dbType, String dbConnStr, int autoSaveRate) {

    this.dbType = dbType;
    this.dbConnStr = dbConnStr;
    this.autoSaveRate = autoSaveRate;
  }

  /**
   * Sets the autoSaveRate.
   *
   * @param autoSaveRate The autoSaveRate to set.
   */
  public void setAutoSaveRate(int autoSaveRate) {
    this.autoSaveRate = autoSaveRate;
  }

  /**
   * Sets the dbConnStr.
   *
   * @param dbConnStr The dbConnStr to set.
   */
  public void setDbConnStr(String dbConnStr) {
    this.dbConnStr = dbConnStr;
  }

  /**
   * Sets the dbType.
   *
   * @param dbType The dbType to set.
   */
  public void setDbType(String dbType) {
    this.dbType = dbType;
  }


}

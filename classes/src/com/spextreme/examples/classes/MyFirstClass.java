package com.spextreme.examples.classes;

/**
 * My First class.
 */
public class MyFirstClass {

  /**
   * Default constructs this object.
   */
  public MyFirstClass() {

  }

  /**
   * Alternate constructs this object.
   *
   * @param value a value to set on this class during construction.
   */
  public MyFirstClass(int value) {

  }
}

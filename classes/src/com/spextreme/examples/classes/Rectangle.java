package com.spextreme.examples.classes;

/**
 *
 */
public class Rectangle {
  int height = 0;
  int width = 0;
  int x = 0;
  int y = 0;

  /**
   * {@inheritDoc}.
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Rectangle other = (Rectangle) obj;
    if (height != other.height) {
      return false;
    }
    if (width != other.width) {
      return false;
    }
    if (x != other.x) {
      return false;
    }
    if (y != other.y) {
      return false;
    }
    return true;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + height;
    result = (prime * result) + width;
    result = (prime * result) + x;
    result = (prime * result) + y;
    return result;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public String toString() {

    return String.format("x: %d, y: %d, w: %d, h: %d, a: %d", x, y, height, width, height * width);
  }
}

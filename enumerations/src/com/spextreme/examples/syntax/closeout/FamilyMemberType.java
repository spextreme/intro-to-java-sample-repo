package com.spextreme.examples.syntax.closeout;

/**
 * A representation of a family member type.
 */
public enum FamilyMemberType {
  /**
   * A child of the parents.
   */
  Child,
  /**
   * Represents the father of a family.
   */
  Father,
  /**
   * Represents the mother of a family.
   */
  Mother,
  /**
   * Undetermined type.
   */
  Other;
}

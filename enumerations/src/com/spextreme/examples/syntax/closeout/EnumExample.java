package com.spextreme.examples.syntax.closeout;

/**
 * Examples for enum usage.
 */
public class EnumExample {

  /**
   * Shows the name method.
   *
   * @param member The member to show it for.
   */
  public static void enumName(FamilyMemberType member) {
    System.out.println("I am a " + member.name());
  }

  /**
   * Show the ordinal method.
   *
   * @param member The member to show it for.
   */
  public static void enumOrdinal(FamilyMemberType member) {
    System.out.println("I am number " + member.ordinal());
  }

  /**
   * Use an enum used in a condition case.
   *
   * @param member The member to show it for.
   */
  public static void ifWithEnum(FamilyMemberType member) {

    if ((member == FamilyMemberType.Mother) || (member == FamilyMemberType.Father)) {
      System.out.println("I'm a parent");
    } else if (member == FamilyMemberType.Child) {
      System.out.println("I'm a child");
    } else {
      System.out.println("Undetermined");
    }
  }

  /**
   * The main execution of this.
   *
   * @param args The args. Not used.
   */
  public static void main(String[] args) {

    switchWithEnum(FamilyMemberType.Father);
    switchWithEnum(FamilyMemberType.Mother);
    switchWithEnum(FamilyMemberType.Child);
    switchWithEnum(FamilyMemberType.Other);

    System.out.println();

    ifWithEnum(FamilyMemberType.Mother);
    ifWithEnum(FamilyMemberType.Father);
    ifWithEnum(FamilyMemberType.Child);
    ifWithEnum(FamilyMemberType.Other);

    System.out.println();

    enumName(FamilyMemberType.Mother);
    enumName(FamilyMemberType.Father);
    enumName(FamilyMemberType.Child);
    enumName(FamilyMemberType.Other);

    System.out.println();

    valuesExample();

    System.out.println();

    enumOrdinal(FamilyMemberType.Mother);
    enumOrdinal(FamilyMemberType.Father);
    enumOrdinal(FamilyMemberType.Child);
    enumOrdinal(FamilyMemberType.Other);

    System.out.println();

    System.out.println("Value of got: " + FamilyMemberType.valueOf("Mother"));

    try {
      FamilyMemberType.valueOf("mother");
    } catch (final Exception e) {
      System.out.println("ValueOf failed because 'mother' doesn't match 'Mother'");
    }

  }

  /**
   * Use an enum in a switch statement.
   *
   * @param member The member to show it for.
   */
  public static void switchWithEnum(FamilyMemberType member) {

    switch (member) {
      case Child:
        System.out.println("I am a child.");
        break;

      case Father:
        System.out.println("I am the father.");
        break;

      case Mother:
        System.out.println("I am the mother.");
        break;

      case Other:
      default:
        System.out.println("undefined");
        break;
    }
  }

  /**
   * Outputs each member by using the values() method.
   */
  public static void valuesExample() {

    for (final FamilyMemberType member : FamilyMemberType.values()) {
      System.out.println(member.name());
    }
  }

}

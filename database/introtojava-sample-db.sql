/* Creates a new database */
CREATE SCHEMA IF NOT EXISTS `introtojava_sample`;

/* Creates a table in the introtojava_sample schema */
CREATE TABLE IF NOT EXISTS `introtojava_sample`.`person` (
  `id` INT NOT NULL,
  `firstname` VARCHAR(64),
  `lastname` VARCHAR(64),
  `email` VARCHAR(128),
  `address` VARCHAR(500),
  PRIMARY KEY (`id`));

/* Insert John Doe into the person table. */
INSERT INTO introtojava_sample.person ( id, firstname, lastname, email, address ) VALUES (1, 'John', 'Deo', 'john.doe@email.com', '123 street rd, some city, NJ 12345');
/* Insert Jane Doe into the person table. */
INSERT INTO introtojava_sample.person ( id, firstname, lastname, email, address ) VALUES (2, 'Jane', 'Doe', 'jane.doe@email.com', '123 street rd, some city, NJ 12345');
/* Insert Bob Smith into the person table. */
INSERT INTO introtojava_sample.person ( id, firstname, lastname, email, address ) VALUES (3, 'Bob', 'Smith', 'bob.smith@email.com', '567 Long Road, alt city, NJ 12346');

/* SELECT all persons */
SELECT * FROM introtojava_sample.person;

/* SELECTS all persons with the last name Doe */
SELECT * FROM introtojava_sample.person WHERE lastname='Doe';

/* Fixes the typo in one of the records */
UPDATE introtojava_sample.person  SET lastname='Doe' WHERE id=1;

/* Find all Doe last names */
SELECT * FROM introtojava_sample.person WHERE lastname='Doe';

/* Delete the record with id of 1 */
DELETE FROM introtojava_sample.person WHERE id=1;

/* Remove the table person */
DROP TABLE introtojava_sample.person;

/*Remove the database schema introtojava_sample */
DROP SCHEMA introtojava_sample;
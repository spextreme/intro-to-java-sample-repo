package com.spextreme.examples.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A sample database connection withe MySQL and JDBC.
 */
public class PersonDatabaseConnection {

  /**
   * A sample connection. This will probably not work in your environment without setting up MySQL
   * and building out the database and a user to connect with.
   *
   * @param args Command args. Not used.
   */
  public static void main(String[] args) {

    Connection conn = null;

    try {
      // Would need a real connection setup with account and database populated.
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/introtojava_sample", "user",
          "password");

      final Statement stmt = conn.createStatement();

      final ResultSet rs = stmt.executeQuery("SELECT * FROM Person");
      while (rs.next()) {
        final int numColumns = rs.getMetaData().getColumnCount();
        for (int i = 1; i <= numColumns; i++) {
          // Different then arrays columns start at 1.
          System.out.println("COLUMN " + i + " = " + rs.getObject(i));
        }
      }
    } catch (final SQLException e) {

      System.out.println("Database error: " + e.getMessage());

    } finally {

      try {

        if ((conn != null) && !conn.isClosed()) {

          conn.close();
        }

      } catch (final SQLException e) {
      }

    }
  }
}

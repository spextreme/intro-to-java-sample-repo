package conditional;

/**
 * Some example of conditional statements.
 */
public class ExampleConditions {

  public static void conditionalOperatorExample(String input) {

    final String output = input.toLowerCase().contains("hello") ? "Hello back" : "Good Bye";

    System.out.println(output);
  }

  public static void ifElseExample(int x, int y) {

    if (x <= y) {
      System.out.println("X is less then Y");
    } else {
      System.out.println("Y is greater then X");
    }
  }

  public static void ifElseIfExample(int x, int y) {

    if (x < y) {
      System.out.println("X is less then Y");
    } else if (x > y) {
      System.out.println("Y is greater then X");
    } else {
      System.out.println("X and Y are equal");
    }
  }

  public static void ifExamples(int x, int y) {

    if (x < y) {
      System.out.println("X is less then Y");
    }

    if (y > x) {
      System.out.println("Y is greater then X");
    }

    if (x != y) {
      System.out.println("X and y are not equal");
    }

    if (x == 1) {
      System.out.println("X equals one");
    }
  }

  public static void main(String[] args) {

    ifExamples(1, 10);
    ifExamples(10, 5);

    System.out.println("\n");

    ifElseExample(1, 10);
    ifElseExample(10, 5);

    System.out.println("\n");

    ifElseIfExample(1, 10);
    ifElseIfExample(10, 5);
    ifElseIfExample(5, 5);

    System.out.println("\n");

    conditionalOperatorExample("Hello World");
    conditionalOperatorExample("No Thanks");

    System.out.println("\n");

    switchExample("Hello");
    switchExample("GoodBye");
    switchExample("How are you today?");
  }

  public static void switchExample(String input) {

    String output = "";

    switch (input.toLowerCase()) {
      case "hello":
        output = "Hello There";
        break;

      case "goodbye":
        output = "See you later.";
        break;

      default:
        output = "Sorry I don't understand that.";
        break;
    }

    System.out.println(output);
  }
}

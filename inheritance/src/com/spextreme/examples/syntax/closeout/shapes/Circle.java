package com.spextreme.examples.syntax.closeout.shapes;

/**
 * The circle shape.
 */
public class Circle extends Shape {

  /**
   * {@inheritDoc}.
   */
  @Override
  public void draw() {

    System.out.println(String.format("I am a Circle (%i, %i).", getX(), getY()));
  }

}

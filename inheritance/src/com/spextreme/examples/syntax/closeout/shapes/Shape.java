package com.spextreme.examples.syntax.closeout.shapes;

/**
 * An abstract class for the shape.
 */
public abstract class Shape implements IShape {
  /**
   * The height in pixels.
   */
  private int height;
  /**
   * The width in pixels.
   */
  private int width;
  /**
   * The x coordinate.
   */
  private int x;
  /**
   * The y coordinate.
   */
  private int y;

  /**
   * Gets the height.
   *
   * @return The height.
   */
  public int getHeight() {
    return height;
  }

  /**
   * Gets the width.
   *
   * @return The width.
   */
  public int getWidth() {
    return width;
  }

  /**
   * Gets the x.
   *
   * @return The x.
   */
  public int getX() {
    return x;
  }

  /**
   * Gets the y.
   *
   * @return The y.
   */
  public int getY() {
    return y;
  }

  /**
   * Sets the height.
   *
   * @param height The height to set.
   */
  public void setHeight(int height) {
    this.height = height;
  }

  /**
   * Sets the width.
   *
   * @param width The width to set.
   */
  public void setWidth(int width) {
    this.width = width;
  }

  /**
   * Sets the x.
   *
   * @param x The x to set.
   */
  public void setX(int x) {
    this.x = x;
  }

  /**
   * Sets the y.
   *
   * @param y The y to set.
   */
  public void setY(int y) {
    this.y = y;
  }

}

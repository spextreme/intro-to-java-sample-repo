package com.spextreme.examples.syntax.closeout.shapes;

/**
 * The polygon shape.
 */
public class Polygon extends Shape {

  /**
   * {@inheritDoc}.
   */
  @Override
  public void draw() {

    System.out.println("I am a Polygon.");
  }

}

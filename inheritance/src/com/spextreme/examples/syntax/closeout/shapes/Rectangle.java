package com.spextreme.examples.syntax.closeout.shapes;

/**
 * The rectangle shape.
 */
public class Rectangle extends Shape {

  /**
   * {@inheritDoc}.
   */
  @Override
  public void draw() {

    System.out.println(String.format("I am a Rectangle (%i, %i, %i, %i).", getWidth(), getHeight(),
        getX(), getY()));
  }

}

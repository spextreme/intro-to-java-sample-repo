package com.spextreme.examples.syntax.closeout.shapes;

/**
 * The rectangle shape.
 */
public class Square extends Shape {

  /**
   * {@inheritDoc}.
   */
  @Override
  public void draw() {

    System.out.println(
        String.format("I am a Square (%i, %i, %i, %i).", getWidth(), getHeight(), getX(), getY()));
  }

}

package com.spextreme.examples.syntax.closeout.shapes;

/**
 * The interface for all shapes to abide by.
 */
public interface IShape {

  /**
   * Draws the shape based on its specific rules.
   */
  void draw();

}

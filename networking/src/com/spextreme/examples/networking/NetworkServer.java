package com.spextreme.examples.networking;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * A simple network server that connects to a port and waits for a message.
 */
public class NetworkServer {
  /**
   * The port this will listen on.
   */
  private int listenPort = -1;

  /**
   * Constructs this object.
   *
   * @param port The port this will listen for messages on.
   */
  public NetworkServer(int port) {

    listenPort = port;
  }

  /**
   * Gets the listenPort.
   *
   * @return The listenPort.
   */
  public int getListenPort() {
    return listenPort;
  }

  /**
   * Starts the server.
   *
   * @throws SocketException Thrown if a socket error occurs before we are able to get up and
   *         running.
   */
  public void start() throws SocketException, UnknownHostException {

    final DatagramSocket socket = new DatagramSocket(getListenPort());

    System.out.println("Server: Listening on Port: " + socket.getLocalPort());

    final byte[] buffer = new byte[256];
    final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

    try {

      System.out.println("Server: Waiting for message...");
      socket.receive(packet);

      System.out.println("Server: Received message.");
      System.out.println(
          "Server: Message from: " + packet.getAddress().getHostAddress() + ":" + packet.getPort());
      System.out.println("Server: Message Content: " + new String(packet.getData()));
      System.out.println("Server: Exiting...");

    } catch (final IOException e) {

      System.out.println("Server: Had an IO error: " + e.getMessage());
    }

  }

}

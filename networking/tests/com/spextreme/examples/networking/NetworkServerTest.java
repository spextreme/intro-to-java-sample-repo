package com.spextreme.examples.networking;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import org.junit.jupiter.api.Test;

/**
 * A test for the datagram network server.
 */
class NetworkServerTest {

  /**
   * Test running the server and getting a message..
   */
  @Test
  void testNetworkServer() throws Exception {
    final int receivePort = 12345;
    final NetworkServer server = new NetworkServer(receivePort);

    final Thread serverThread = new Thread((Runnable) () -> {

      try {

        server.start();

      } catch (final Exception e) {

        System.out.println("Client: Failed to start server with exception: " + e.getMessage());
      }
    }, "NetworkServer");

    serverThread.start();

    Thread.sleep(50);

    final String message = "Hello from the other side!";
    final DatagramPacket datagramMessage = new DatagramPacket(message.getBytes(), message.length(),
        InetAddress.getByName("127.0.0.1"), receivePort);

    final DatagramSocket sendingSocket = new DatagramSocket();

    System.out.println("Client: Sending message.");
    sendingSocket.send(datagramMessage);

    sendingSocket.close();
  }

}

package com.spextreme.examples.exceptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * A set of examples for exceptions.
 */
public class ExceptionSamples {

  public static void main(String[] args) {
    final ExceptionSamples samples = new ExceptionSamples();

    samples.exceptionHandlingTryCatch();

    try {

      samples.exceptionHandlingThrows();

    } catch (final Exception e) {

      System.out.println("Print the full stack trace of an error:");
      e.printStackTrace();

    }
  }

  /**
   * A simple example of a method throwing an exception it creates.
   *
   * @throws BadThingsHappenedException A custom exception we created.
   */
  public void exceptionHandlingThrowException() throws BadThingsHappenedException {

    throw new BadThingsHappenedException("This isn't implmeneted yet");
  }

  /**
   * A simple method to throw an exception.
   *
   * @throws MalformedURLException Thrown from this so the caller has to handle it.
   */
  public void exceptionHandlingThrows() throws MalformedURLException {

    // A URL is any web address and has a fixed format. The
    // below gives an invalid URL to force the object to
    // throw a checked exception.

    new URL("spextreme.gitlab.io/site/courses/intro-to-java/");
  }

  /**
   * Thrown a set of exceptions example.
   *
   * @throws MalformedURLException Thrown from this so the caller has to handle it.
   * @throws URISyntaxException Thrown if the URL conversion fails.
   * @throws FileNotFoundException Thrown if the file isn't found on disk.
   * @throws BadThingsHappenedException A custom exception we created.
   */
  public void exceptionHandlingThrowsMultiple() throws MalformedURLException, URISyntaxException,
      FileNotFoundException, BadThingsHappenedException {

    final URL url = new URL("file:///tmp/file.txt");
    new FileInputStream(new File(url.toURI()));
  }

  /**
   * A simple method to catch any Exception types that are thrown.
   */
  public void exceptionHandlingTryCatch() {

    try {

      // A URL is any web address and has a fixed format. The
      // below gives an invalid URL to force the object to
      // throw a checked exception.

      new URL("spextreme.gitlab.io/site/courses/intro-to-java/");

    } catch (final MalformedURLException me) {
      System.out.println("Url is malformed.");
    } catch (final Exception e) {
      System.out.println("Some error occurred.");
    }

  }

  /**
   * A try catch with finally example.
   */
  public void exceptionHandlingTryCatchFinally() {

    FileInputStream fis = null;

    try {

      fis = new FileInputStream("testfile.txt");
      final int data = fis.read();

      System.out.println("First Byte of Data is: " + data);

    } catch (final Exception e) {

      System.out.println("Had an error finding the file or reading it.");
    } finally {

      if (fis != null) {
        try {

          fis.close();
        } catch (final Exception e2) {
          // ignore not this since we are really done.
        }
      }
    }
  }

  /**
   * A try catch with mulitple exceptions
   */
  public void exceptionHandlingTryCatchMultipe() {

    try {

      // A URL is any web address and has a fixed format. The
      // below gives an invalid URL to force the object to
      // throw a checked exception.

      new URL("spextreme.gitlab.io/site/courses/intro-to-java/");

    } catch (final Exception e) {

      System.out.println("We gave a bad URL.  Should have been "
          + "https://spextreme.gitlab.io/site/courses/intro-to-java/");
    }

  }


  /**
   * A try with resource example.
   */
  public void exceptionHandlingTryWithResource() {

    try (FileInputStream fis = new FileInputStream("testfile.txt")) {

      final int data = fis.read();

      System.out.println("First Byte of Data is: " + data);

    } catch (final Exception e) {

      System.out.println("Had an error finding the file or reading it.");
    }
  }
}

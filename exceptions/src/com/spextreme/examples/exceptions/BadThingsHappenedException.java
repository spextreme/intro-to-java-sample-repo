package com.spextreme.examples.exceptions;

/**
 * A custom exception for when bad things happen.
 */
public class BadThingsHappenedException extends Exception {

  /**
   * Default constructs this object.
   */
  public BadThingsHappenedException() {

  }

  /**
   * Constructs this object with a message.
   *
   * @param message The message to add to this exception.
   */
  public BadThingsHappenedException(String message) {

  }
}

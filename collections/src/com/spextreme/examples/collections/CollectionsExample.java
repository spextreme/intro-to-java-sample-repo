package com.spextreme.examples.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *Example of collections.
 */
public class CollectionsExample {

  /**
   * @param args
   */
  public static void main(String[] args) {

    final CollectionsExample example = new CollectionsExample();

    example.forExample();
    example.forEachExample();
    example.iteratorExample();

  }

  /**
   * A loop on an {@link ArrayList} using the for each loop.
   */
  public void forEachExample() {

    final List<String> list = new ArrayList<>();

    list.add("Hello");
    list.add("World");
    list.add("one");

    for (final String str : list) {

      System.out.println(str);
    }
  }

  /**
   * A loop on an {@link ArrayList} using the for loop with an index.
   */
  public void forExample() {

    final List<String> list = new ArrayList<>();

    list.add("Hello");
    list.add("World");
    list.add("one");

    for (int i = 0; i < list.size(); i++) {

      System.out.println(list.get(i));
    }
  }

  /**
   * A loop on an {@link ArrayList} using an {@link Iterator}.
   */
  public void iteratorExample() {

    final List<String> list = new ArrayList<>();

    list.add("Hello");
    list.add("World");
    list.add("one");

    for (final Iterator<String> intr = list.iterator(); intr.hasNext();) {

      System.out.println(intr);
    }
  }
}

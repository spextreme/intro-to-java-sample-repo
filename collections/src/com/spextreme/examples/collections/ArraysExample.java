package com.spextreme.examples.collections;

/**
 * Example array work.
 */
public class ArraysExample {

  /**
   * Runs the examples.
   *
   * @param args Not used.
   */
  public static void main(String[] args) {
    final ArraysExample examples = new ArraysExample();

    examples.arrayExample1();

    examples.arrayExample2();
  }

  /**
   * Example 1. Creates array, adds 5 elements and then prints it out using strings method to
   * create from a char array.
   */
  public void arrayExample1() {
    final char[] charArray = new char[5];

    charArray[0] = 'h';
    charArray[1] = 'e';
    charArray[2] = 'l';
    charArray[3] = 'l';
    charArray[4] = 'o';

    System.out.println(new String(charArray));
  }

  /**
   * Same as example 1 but simplified creation and using a loop to output.
   */
  public void arrayExample2() {
    final char[] charArray = {'h', 'e', 'l', 'l', 'o'};

    for (int i = 0; i < charArray.length; i++) {
      System.out.print(charArray[i]);
    }
  }
}

package com.spextreme.examples.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple generics example.
 */
public class GenericsExample {

  /**
   * Runs the two examples.
   *
   * @param args not used.
   */
  public static void main(String[] args) {

    final GenericsExample example = new GenericsExample();

    example.genericsExample1();
    example.genericsExample2();
  }

  /**
   * An example to show not using generics.
   */
  public void genericsExample1() {

    final List str = new ArrayList<>();

    str.add("Hello");
    str.add("World");

    for (final Object obj : str) {

      // Casting from object to string only works cause
      // our list is made up of only strings.
      final String objStr = (String) obj;

      System.out.println(objStr);
    }
  }

  /**
   * An example using generics.
   */
  public void genericsExample2() {

    final List<String> str = new ArrayList<>();

    str.add("Hello");
    str.add("World");

    for (final String objStr : str) {

      System.out.println(objStr);
    }
  }
}

package edu.rowan.classwork;

/**
 * The interface of any users.
 */
public interface IUser {

  /**
   * Gets the email address.
   *
   * @return The user email address.
   */
  String getEmail();

  /**
   * Gets the username for this user.
   *
   * @return The user name.
   */
  String getUsername();
}

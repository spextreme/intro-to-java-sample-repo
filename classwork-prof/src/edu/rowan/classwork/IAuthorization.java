package edu.rowan.classwork;

/**
 * An interface for any authorization instances.
 */
public interface IAuthorization {
  /**
   * Checks that the user is authenticated.
   *
   * @param user The user to check.
   * @return <code>true</code> if the user is authenticated.
   */
  boolean isAuthenticated(IUser user);
}

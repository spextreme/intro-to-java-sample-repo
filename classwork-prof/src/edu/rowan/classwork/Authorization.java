package edu.rowan.classwork;

/**
 * An authorization object.
 */
public class Authorization implements IAuthorization {

  /**
   * Constructs this object.
   */
  public Authorization() {

  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public boolean isAuthenticated(IUser user) {

    return false;
  }

}

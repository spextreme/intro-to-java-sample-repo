package edu.rowan.classwork;

/**
 * A general user implementation.
 */
public class User implements IUser {

  /**
   * The email address.
   */
  private final String email;
  /**
   * The user name.
   */
  private final String username;

  /**
   * Constructs this object.
   *
   * @param username The user login name. Can not be null or empty.
   * @param email The email address. Can be not be null or empty.
   *
   * @throws IllegalArgumentException Thrown if the username or email are null or empty.
   */
  public User(String username, String email) {

    this.username = username;
    this.email = email;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public String getEmail() {

    return email;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public String getUsername() {

    return username;
  }

}

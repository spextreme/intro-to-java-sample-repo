package edu.rowan.classwork;

/**
 * Represents an administrative user.
 */
public class Administrator extends User {

  /**
   * Constructs this object.
   *
   * @param username The user login name. Can not be null or empty.
   * @param email The email address. Can be not be null or empty.
   *
   * @throws IllegalArgumentException Thrown if the username or email are null or empty.
   */
  public Administrator(String username, String email) {

    super(username, email);
  }

}

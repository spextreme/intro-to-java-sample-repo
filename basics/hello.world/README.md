# Intro to Java - First Program

This is the a simple hello world example.  Nothing more to it.

## Compiling
 
javac HelloWorld.java


## Running

This required it to have been compiled to a *.class file.

java HelloWorld


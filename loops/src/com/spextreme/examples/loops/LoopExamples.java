package com.spextreme.examples.loops;

/**
 * A collection of loop examples for Java.
 */
public class LoopExamples {

  /**
   * This will run once and then end since its a post-test model and the first condition check will
   * make the condition greater than 1.
   */
  public static void doWhileLoopExample() {

    int counter = 0;

    do {

      System.out.println("Only once");
      counter++;
    } while (counter < 1);
  }

  /**
   * A for each example that outputs an array of strings.
   */
  public static void forEachExample() {

    final String[] array = new String[] {"Hello", "World", "!", "Loops", "are", "powerful"};
    System.out.println("For Each item: ");

    for (final String str : array) {
      System.out.println(String.format("\t %s", str));
    }
  }

  /**
   * A simple index for loop example.
   */
  public static void forLoopCounter() {

    System.out.println("For Counter");

    for (int i = 0; i < 20; i++) {
      System.out.println("\t" + i);
    }
  }

  /**
   * A for loop example that initializes and updates multiple variables.
   */
  public static void forLoopMultipleCounter() {

    System.out.println("Multiple Var For Counter");

    for (int i = 0, j = 50; (i < 10) || (j > 10); i++, j--) {
      System.out.println(String.format("\ti: %d, j: %d", i, j));
    }
  }

  /**
   * This is an infinite loop that will never end. It is missing the counter++ to make it properly
   * finish.
   */
  public static void infinitLoop() {

    final int counter = 0;

    while (counter < 20) {

      System.out.println(counter + " - Done Yet...");
    }
  }


  /**
   * This loops a fixed number of times and prints out the counter. This will work properly since
   * the counter is incremented.
   */
  public static void infinitLoopFixed() {

    int counter = 0;

    while (counter < 20) {

      System.out.println(counter + " - Done Yet...");
      counter++; // This was missing
    }
  }

  /**
   * The main executable.
   *
   * @param args The arguments...not used.
   */
  public static void main(String[] args) {

    infinitLoopFixed();

    // Do not run this one...it will never end
    // infinitLoop();

    System.out.println();

    doWhileLoopExample();

    System.out.println();

    forLoopCounter();

    System.out.println();

    forLoopMultipleCounter();

    System.out.println();

    forEachExample();

    System.out.println();

    whileWithBreakExample();

    System.out.println();

    whileWithContinueExample();

  }

  /**
   * A bad coding habit but an example of the break statement to get out of an infinite loop.
   */
  public static void whileWithBreakExample() {

    int counter = 0;

    System.out.println("While with break: ");
    while (true) {

      if (counter > 20) {
        break;
      }

      System.out.println(counter);
      counter++;

    }
  }

  /**
   * A bad coding habit but an example of the continue statement to skip every odd number.
   */
  public static void whileWithContinueExample() {

    int counter = -1;

    System.out.println("While with Continue: ");
    while (counter < 20) {

      counter++;

      if ((counter % 2) != 0) {
        continue;
      }

      System.out.println(counter);

    }
  }
}

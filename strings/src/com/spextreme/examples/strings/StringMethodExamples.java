package com.spextreme.examples.strings;

/**
 * Some examples of string methods.
 */
public class StringMethodExamples {

  /**
   * Runs all methods.
   *
   * @param args Not used
   */
  public static void main(String[] args) {

    StringMethodExamples.stringEquality();
    System.out.println();
    StringMethodExamples.stringContains();
    System.out.println();
    StringMethodExamples.stringLength();
    System.out.println();
    StringMethodExamples.stringIndexOf();
    System.out.println();
    StringMethodExamples.stringLastIndexOf();
    System.out.println();
    StringMethodExamples.stringEmptyBlank();
    System.out.println();
    StringMethodExamples.stringSubstring();
    System.out.println();
    StringMethodExamples.stringLowerUpper();
    System.out.println();
    StringMethodExamples.stringValueOf();
  }

  /**
   * An example of the contains method.
   */
  public static void stringContains() {
    final String myString = "The cow jumped over the moon";
    final String contains = "jump";
    final String notContains = "hello";

    System.out.println(
        "\"" + myString + "\".contains(\"" + contains + "\"); // " + myString.contains(contains));
    System.out.println("\"" + myString + "\".contains(\"" + notContains + "\"); // "
        + myString.contains(notContains));
  }


  /**
   * An example of the isEmpty and isBlank method.
   */
  public static void stringEmptyBlank() {
    final String emptyString = "";
    final String blankString = "\t "; // \t is a tab
    final String aString = "Hello World!";

    System.out.println("\"" + emptyString + "\".isEmpty(); // " + emptyString.isEmpty());
    System.out.println("\"" + blankString + "\".isEmpty(); // " + blankString.isEmpty());
    System.out.println("\"" + aString + "\".isEmpty(); // " + aString.isEmpty());
    System.out.println("\"" + emptyString + "\".isBlank(); // " + emptyString.isBlank());
    System.out.println("\"" + blankString + "\".isBlank(); // " + blankString.isBlank());
    System.out.println("\"" + aString + "\".isBlank(); // " + aString.isBlank());
  }

  /**
   * A method to show the equals and equalsIgnoreCase methods.
   */
  public static void stringEquality() {

    final String helloLower1 = "hello";
    final String helloLower2 = "hello";
    final String helloUpper = "Hello";
    final String worldLower = "world";

    System.out.println("\"" + helloLower1 + "\".equals(\"" + helloUpper + "\"); // "
        + helloLower1.equals(helloUpper));
    System.out.println("\"" + helloLower1 + "\".equals(\"" + helloLower2 + "\"); // "
        + helloLower2.equals(helloLower2));
    System.out.println("\"" + helloLower1 + "\".equals(\"" + worldLower + "\"); // "
        + helloLower1.equals(worldLower));
    System.out.println("\"" + helloLower1 + "\".equalsIgnoreCase(\"" + helloUpper + "\"); // "
        + helloLower1.equalsIgnoreCase(helloUpper));
  }

  /**
   * An example of the indexOf method.
   */
  public static void stringIndexOf() {
    final String myString = "Hello World!";

    System.out.println("\"" + myString + "\".indexOf(\"World\"); // " + myString.indexOf("World"));
    System.out.println("\"" + myString + "\".indexOf(\" \"); // " + myString.indexOf(" "));
    System.out.println("\"" + myString + "\".indexOf(\"Intro\"); // " + myString.indexOf("Intro")); // not
  }

  /**
   * An example of the lastIndexOf method.
   */
  public static void stringLastIndexOf() {
    final String myString =
        "Hello world!  What a big world it can be and it may go round and round.";

    System.out.println(
        "\"" + myString + "\".lastIndexOf(\"world\"); // " + myString.lastIndexOf("world"));
    System.out.println(
        "\"" + myString + "\".lastIndexOf(\"round\"); // " + myString.lastIndexOf("round"));
    System.out
        .println("\"" + myString + "\".lastIndexOf(\"not\"); // " + myString.lastIndexOf("not"));
  }

  /**
   * An example of the length method.
   */
  public static void stringLength() {
    final String myString = "The cow jumped over the moon";

    System.out.println("\"The cow jumped over the moon\".length(); // " + myString.length());
  }

  /**
   * An example of the toLowerCase/toUpperCase method.
   */
  public static void stringLowerUpper() {
    final String fullString = "Hello World!";

    System.out.println("\"" + fullString + "\".toLowerCase(); // " + fullString.toLowerCase());
    System.out.println("\"" + fullString + "\".toUpperCase(); // " + fullString.toUpperCase());
  }

  /**
   * An example of the substring method.
   */
  public static void stringSubstring() {
    final String fullString = "This is a string. It contains two sentences.";

    System.out.println("\"" + fullString + "\".substring(18); //" + fullString.substring(18));
    System.out.println("\"" + fullString + "\".substring(0,17); //" + fullString.substring(0, 17));

    final int index = fullString.indexOf(".") + 1;
    System.out.println(
        "\"" + fullString + "\".substring(" + index + "); //" + fullString.substring(index));
  }

  /**
   * An example of the valueOf methods.
   */
  public static void stringValueOf() {

    System.out.println("String.valueOf(true); // " + String.valueOf(true));
    System.out.println("String.valueOf(false); // " + String.valueOf(false));

    System.out.println("String.valueOf('a'); // " + String.valueOf('a'));
    System.out.println("String.valueOf(new char[] {'a', 'b', 'c'}); // "
        + String.valueOf(new char[] {'a', 'b', 'c'}));

    System.out.println("String.valueOf(342.92384d); // " + String.valueOf(342.92384d));
    System.out.println("String.valueOf(3.1415f); // " + String.valueOf(3.1415f));
    System.out.println("String.valueOf((short)1); // " + String.valueOf((short) 1));
    System.out.println("String.valueOf(13); // " + String.valueOf(13));
    System.out
        .println("String.valueOf(1234567890123414L); // " + String.valueOf(1234567890123414L));
    System.out.println("String.valueOf(object); // " + String.valueOf(new Object()));
  }
}

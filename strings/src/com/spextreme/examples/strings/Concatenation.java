package com.spextreme.examples.strings;

/**
 * A class containing concatenation examples.
 */
public class Concatenation {

  public static void main(String[] args) {
    final Concatenation concat = new Concatenation();

    concat.helloWorldConcatenation();

    concat.stringFormatConcatenation();

    concat.stringFormatFormat();
  }

  public void helloWorldConcatenation() {

    final String a = "Hello";
    final String b = "World";

    final String newString = a + " " + b;

    System.out.println(newString);
  }

  public void stringFormatConcatenation() {
    final String pre = "The name of the professor for";
    final String className = "Intro to Java";
    final int courseNum = 1140;
    final String post = "is";
    final String name = "Steve";

    final String finalString =
        pre + " cs0" + courseNum + "-" + className + " " + post + " " + name + ".";

    System.out.println(finalString);
  }

  public void stringFormatFormat() {
    final String pre = "The name of the professor for";
    final String className = "Intro to Java";
    final int courseNum = 1140;
    final String post = "is";
    final String name = "Steve";

    final String finalString =
        String.format("%s cs0%d-%s %s %s.", pre, courseNum, className, post, name);

    System.out.println(finalString);
  }
}

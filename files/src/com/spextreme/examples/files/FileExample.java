package com.spextreme.examples.files;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * File input/output examples.
 */
public class FileExample {

  public static void main(String[] args) {
    final FileExample example = new FileExample();

    example.inputFileContents();

    example.outputFileContents();

    example.inputChainingFileContents();

  }

  /**
   * An example of chaining input streams together.
   */
  public void inputChainingFileContents() {

    try (BufferedInputStream inputStream =
        new BufferedInputStream(new FileInputStream("InputFile.txt"))) {

      int b = 0;

      while ((b = inputStream.read()) != -1) {
        System.out.print((char) b);
      }
      System.out.println();

    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * An example of reading a character at a time from a file.
   */
  public void inputFileContents() {

    try (FileInputStream inputStream = new FileInputStream("InputFile.txt")) {

      int b = 0;

      while ((b = inputStream.read()) != -1) {
        System.out.print((char) b);
      }
      System.out.println();

    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * An example of writing characters out to a file.
   */
  public void outputFileContents() {

    try (FileOutputStream outputStream = new FileOutputStream("OutputFile.txt")) {

      outputStream.write("Put me in a file please.".getBytes());
      System.out.println("Wrote file to: OutputFile.txt");

    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

}

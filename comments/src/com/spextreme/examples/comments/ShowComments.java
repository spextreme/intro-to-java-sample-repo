package com.spextreme.examples.comments;

/**
 * A class level comment.
 */
public class ShowComments {

  /**
   * A comment for a methods with an input parameter and return type description.
   *
   * @param param The parameter comment.
   * @return The return comment.
   */
  public int methodComment(int param) {

    return param + 1;
  }
}

# Commenting and Javadoc Example

This is a simple example of commenting and generating Javadoc from the code.

## Javadoc

To run the Javadoc tool on this code, make sure you have the JDK installed and on your path then type

javadoc --version

If that prints out the version of javadoc you should be good.  Then you will want to run the javadoc command on the source file.

javadoc src/com.spextreme.examples.comments/ShowComments.java

After running it the current directory should have a large collection of content associated with it.  You can open the index.html file to review what was generated with your web browser.

package com.spextreme.examples.annotations;

import java.beans.BeanProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@MyCustomAnnotation(owner = "SP extreme", permissions = "Use as an example only", required = true)
public class ExampleAnnotationsUse {

  @SuppressWarnings({"rawtypes", "unchecked"})
  public void aMethodWithAWarning() {

    // This would report a warning in Eclipse because we didn't provide the
    // list with a type (rawtype warning).
    // Using the annotation we can tell Eclipse to ignore this warning.
    final List unTypedList = new ArrayList();

    // Unchecked is reported when passing a string into a list that is not typed as a String.
    // Rawtype lists are actually Object based since anything may be place in the list then.
    unTypedList.add("Hello");
  }

  @BeanProperty(description = "A simple java bean example", expert = false, required = false)
  private String myBean() {

    return "BeanProperty";
  }

  @Deprecated
  public void outdatedMethod() {
    // This is no longer needed.
  }


}

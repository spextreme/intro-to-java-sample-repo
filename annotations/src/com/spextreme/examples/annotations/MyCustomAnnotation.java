package com.spextreme.examples.annotations;

public @interface MyCustomAnnotation {
  String owner();

  String permissions();

  boolean required();
}
